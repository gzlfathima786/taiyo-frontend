 ## Tools 💻
- HTML,
- CSS,
- React,
- Redux,
- Redux-Thunk,
- TypeScript
- Tailwind css,
- React-simple-maps
- React-query

## Link  🔗
https://taiyo-frontend.vercel.app/contact

## How to run locally ?
 1. clone it
 2. Install node_modules
     ```
      npm i
     ```
 3. start it
     ```
     npm run start
     ```


## Features ✨
`
- Cool Topbar and Side Navigation bar.
- Create/Read/Update/Delete Contact.
- Cool Dashboard of Charts and Maps.
- Popup with country specific data in world map.


## Screenshots  📸

![Alt Text](public/images/create-contact.png "Image Title")

![Alt Text](public/images/contact-form.png "Image Title")

![Alt Text](public/images/contact-list.png "Image Title")

![Alt Text](public/images/world-data.png "Image Title")

![Alt Text](public/images/world-map.png "Image Title")

![Alt Text](public/images/world-popup.png "Image Title")


