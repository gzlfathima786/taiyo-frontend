import { useWorldData } from '../Hooks/useWorldData'
import { Link } from "react-router-dom"

function Charts(): JSX.Element {
  const fun = useWorldData()

  if (fun.isLoading) {
    return <div className="text-3xl font-serif font-bold h-[10vh] w-[10vw] flex justify-center items-center m-auto mt-[40vh]">Loading...</div>;
  }

  if (fun.error) {
    return <div>Error: {(fun.error as Error).message}</div>;
  }

  console.log(fun.data, 'fun');

  return (

    <>

      <div className='text-center flex flex-col justify-center items-center gap-4 m-auto h-[90svh] '>
        <div className=' shadow-xl bg-white px-4 py-2 rounded-lg'>
          <p className='font-bold text-xl font-[cursive]'>World Data</p>
          <p>Total number of active: <span className="ml-7">{(fun.data as { active: number }).active}</span></p>
          <p>Recovered Cases: <span className="ml-[5vw]">{(fun.data as { recovered: number }).recovered}</span></p>
          <p>Deaths: <span className="ml-[10vw]">{(fun.data as { deaths: number }).deaths}</span></p>
        </div>

        <Link to="/charts/countries"><p className='shadow-xl border-2 border-white px-2 py-1 bg-green-400 rounded-lg font-bold text-2xl'>Go to Country Specific Data</p></Link>
      </div>
    </>
  )
}

export default Charts
