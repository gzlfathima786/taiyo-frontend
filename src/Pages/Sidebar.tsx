import React from "react";
import { Link, Outlet } from "react-router-dom";
function Sidebar() {
  return (
    <div className="flex relative">
      <div className="flex flex-col border-2 border-black w-[30vw] h-[90svh] md:w-2/12 lg:w-1/12 sticky left-0 text-sm font-bold">
        <Link to="/contact">
          <p className="underline text-[#4343F7] text-center my-2">Contact</p>
        </Link>
        <div className="bg-black h-1"></div>
        <Link to="/charts"><p className="underline text-[#4343F7]  text-center my-2">Charts and Maps</p></Link>
        <div className="bg-black h-1"></div>
        <div><p className=" text-center my-2">Sidebar</p></div>
      </div>
      <div className="h-[90svh] w-11/12 bg-[#ECE9E4]">
      <Outlet />
      </div>
      
    </div>
  );
}

export default Sidebar;
