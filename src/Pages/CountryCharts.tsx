import React, { useState } from "react";
import { useCountryData } from "../Hooks/useCountryData";
import {
  ComposableMap,
  Geographies,
  Geography,
  ZoomableGroup,
} from "react-simple-maps";
import map from "../map.json"
const CountryCharts: React.FC = () => {
  const { isLoading, error, data } = useCountryData();
  const [selectedCountry, setSelectedCountry] = useState<string | null>(null);

  const [popupData, setPopupData] = useState(null);
  const [isOpen, setIsOpen] = useState(false);

  if (isLoading) {
    return <div className="text-3xl font-serif font-bold h-[10vh] w-[10vw] flex justify-center items-center m-auto mt-[40vh]">Loading...</div>;
  }

  if (error) {
    return <div>Error: {(error as Error).message}</div>;
  }

  const handleClick = (geo: any) => {

    const countryName = geo.properties["Alpha-2"];
    setIsOpen(true);
    setPopupData(data?.find((el: any) => el.countryInfo.iso2 === countryName));
    setSelectedCountry(countryName);
  };

  // console.log(popupData, 'popupData');

  return (
    <>
      <div className="no-scrollbar overflow-hidden w-full max-h-[90svh]">
        <ComposableMap projection="geoMercator">
          <ZoomableGroup center={[0, 0]} zoom={1}>
            <Geographies geography={map}>
              {({ geographies }) =>
                geographies?.map((geo) => {
                  const item = data?.find(
                    (d: any) => d.countryInfo.iso2 === geo.properties["Alpha-2"]
                  );
                  return (
                    <Geography
                      key={geo.rsmKey}
                      geography={geo}
                      fill={
                        selectedCountry === geo.properties["Alpha-2"]
                          ? "#FF5722"
                          : item
                            ? "#AAA"
                            : "#DDD"
                      }
                      stroke="#FFF"
                      onClick={() => handleClick(geo)}
                    />
                  );
                })
              }
            </Geographies>
          </ZoomableGroup>
        </ComposableMap>
      </div>
      {(popupData && isOpen) ? (
        <div className="fixed top-[40vh] left-[35vw] md:left-[40vw] bg-white h-auto py-4 px-4 w-[52vw] flex flex-col md:w-[22vw]">
          <p>Country Name: <span className="ml-20">{(popupData as { country: string }).country}</span></p>
          <p>Total number of active: <span className="ml-7">{(popupData as { active: number }).active}</span></p>
          <p>Recovered Cases: <span className="ml-[5vw]">{(popupData as { recovered: number }).recovered}</span></p>
          <p>Deaths: <span className="ml-[10vw]">{(popupData as { deaths: number }).deaths}</span></p>
        
          <button className='float-right px-2 py-1 bg-red-500 text-white font-[cursive] font-semibold mt-4' onClick={() => setIsOpen(false)}>close</button>
        </div>
      ) : (
        ""
      )}
    </>
  );
};

export default CountryCharts;