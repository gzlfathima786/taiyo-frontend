import { useQuery } from 'react-query'

export function useCountryData() {
  return useQuery('data', async () => {
    const response = await fetch('https://disease.sh/v3/covid-19/countries')
    const data = await response.json()

    // console.log(data , 'country-data');
    return data
  })
}
