import { useQuery } from 'react-query'

export function useWorldData() {
  return useQuery('worldData', async () => {
    const response = await fetch('https://disease.sh/v3/covid-19/all')
    const data = await response.json()
    // console.log(data , 'world-data');
    return data
  })
}
