
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import './App.css';
import Sidebar from './Pages/Sidebar';
import ShowContact from './Pages/ShowContact';
import Topbar from './Pages/Topbar';
import CreateContact from './Pages/CreateContact';
import EditContact from './Pages/EditContact';
import Charts from './Pages/Charts';
import CountryCharts from './Pages/CountryCharts';



function App() {
  return (
    <>
      <BrowserRouter >
        <Topbar />
        <Routes>
          <Route path='/' element={<Sidebar />}>
            <Route path='/contact' element={<ShowContact />} ></Route>
            <Route path='/charts' element={<Charts />} ></Route>
            <Route path='/contact/create' element={<CreateContact />}></Route>
            <Route path='/contact/edit/:id' element={<EditContact />}></Route>
            <Route path='/charts/countries' element={<CountryCharts />} ></Route>
            <Route path="" element={<Navigate to="/contact" />} />
          </Route>
        </Routes>

      </BrowserRouter>
    </>
  );
}

export default App;
