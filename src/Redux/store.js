import { applyMiddleware, combineReducers, legacy_createStore } from "redux";
import thunk from "redux-thunk";

import {contactReducer} from "./Contact/contact.reducer"

export const store = legacy_createStore(
  combineReducers({
    contact: contactReducer,
  }),
  applyMiddleware(thunk)
);
